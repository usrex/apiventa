import { Response, Request } from "express";
import { pool } from '../database';
import { QueryResult } from 'pg';

export const getUsers = async (req: Request, res: Response): Promise<Response> => {
    try {
        const response: QueryResult<any> = await pool.query('SELECT * FROM usuario');
        
        return res.status(200).json(response.rows);

    } catch (error) {
        console.log(error);
        return res.status(500).json("internal server error");
    }
};
/*
export const createUser = async (req: Request, res: Response) => {
    
    const { nombre,apellidos,correo, pasword,rol_usuario,url_image} = req.body;
    const response = await pool.query('insert into Usuario(nombre, apellidos, correo, pasword, rol_usuario, url_image) VALUES ($1, $2,$3,$4,$5,$6)', [nombre,apellidos,correo, pasword,rol_usuario,url_image]);
    console.log(response);
    res.status(200).json(
        {
            message: "usuario agregado con exito",
            body: response
        }
    );    
};


export const getUserById = async (req: Request, res: Response): Promise<Response> => {
    const id = parseInt(req.params.id);
    console.log(id);
    console.log(req.params);
    const response: QueryResult = await pool.query('SELECT * FROM usuario WHERE id_usuario = $1', [id]);
    return res.json(response.rows);
};


export const updateUser = async (req: Request, res: Response) => {
    console.log("put user");
    // actualizar
    // nombre,apellidos,correo, pasword
    const id = parseInt(req.params.id);
    const { nombre,apellidos,correo, pasword } = req.body;
    //console.log(id);
    //console.log(nombre,apellidos,correo, pasword);
    try {
        
        const response = await pool.query('UPDATE usuario SET nombre = $1, apellidos = $2, correo = $3, pasword = $4 WHERE id_usuario = $5', [
            nombre,apellidos,correo, pasword, id
        ]);
        
    res.status(200).json({
        message:'User Updated Successfully',
        data: response.oid   
    });
    } catch (error) {
        res.status(500).json({
            message:'User NOT Updated Successfully',
        }); 
    }
 
};

export const deleteUser = async (req: Request, res: Response) => {
    const id = parseInt(req.params.id);
    await pool.query('DELETE FROM usuario where id_usuario = $1', [
        id
    ]);
    res.json(`User ${id} deleted Successfully`);
};

*/

