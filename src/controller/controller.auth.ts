import { Request, Response } from "express";
import { IsAdmin } from "functions/AuthRol";
import jwt from "jsonwebtoken";
import { QueryResult } from "pg";
import { pool } from "../database";
import { encrypt, comparePassword } from "../functions/encryptPassword";

export const signup = async (req: Request, res: Response): Promise<Response> => {
    const { nombre, apellidos, correo, rol_usuario, url_image } = req.body;
    const pasword = await encrypt(req.body.pasword);
    try {
        const response: QueryResult = await pool.query('insert into Usuario(nombre, apellidos, correo, pasword, rol_usuario, url_image) VALUES ($1, $2,$3,$4,$5,$6)', [nombre, apellidos, correo, pasword, rol_usuario, url_image]);
        //console.log(response);
        try {
            const querysearchid: QueryResult = await
                pool.query(
                    'SELECT * FROM Usuario WHERE nombre = $1 and correo = $2 and pasword = $3 and rol_usuario = $4', [nombre, correo, pasword, rol_usuario]);
            const id = querysearchid.rows.map(
                (data) => {
                    return data.id_usuario;
                }
            );
            //console.log(id[0]);
            const token: string = jwt.sign({ id: id[0] }, "TOkEN_TESTING");
            return res.header("token-auth", token).status(200).json(
                {
                    message: "usuario agregado con exito",
                    body: { nombre, apellidos, correo, rol_usuario, url_image, pasword },
                    numeroInserts: response.rows
                }
            );
        } catch (error) {
            console.log("data no encontrada");
            return res.status(500).json("Internal Server Error")
        }
    } catch (error) {
        console.log("no insert");
        return res.status(500).json("internal server error")
    }

};
export const signin = async (req: Request, res: Response): Promise<Response> => {
    const { correo, pasword } = req.body;
    console.log(correo);
    console.log(pasword);
    try {

        const response: QueryResult = await pool.query('SELECT * FROM Usuario WHERE correo = $1', [correo]);

        const passwordCrypt = response.rows.map((data) => { return { pass: data.pasword, id: data.id_usuario } });
        //console.log(passwordCrypt);
        const compar: boolean = await comparePassword(pasword, passwordCrypt[0].pass);// ? errore compare 

        if (compar) {
            const token: string = jwt.sign({ id: passwordCrypt[0].id }, "TOkEN_TESTING");
            return res.header('auth-token', token)
                .status(200)
                .json(
                    {
                        message: "inicio con exito",
                        data: response.rows
                    });
        } else
            return res.status(400).json("contraseño o email incorrectos");

    } catch (error) {
        return res.send({ message: 'internal server error', error: error });
    }
};
export const profile = async (req: Request, res: Response): Promise<Response> => {
    //console.log(req.headers['auth-token']);
    //console.log(req.userId);
    try {
        const response: QueryResult = await pool.query('SELECT * FROM Usuario WHERE id_usuario = $1', [req.userId]);
        if (response.rowCount != 0) {
            return res.status(200).json(response.rows);
        } else
            return res.status(404).json('Data not found')
    } catch (error) {
        return res.status(400).json('internal server error')
    }

};



export const editProfile = async (req: Request, res: Response) => {
    console.log("put user");
    // actualizar
    // nombre,apellidos,correo, pasword
    const id = parseInt(req.userId);
    const { nombre, apellidos, correo, pasword } = req.body;
    //console.log(id);
    //console.log(nombre,apellidos,correo, pasword);
    try {
        const response = await pool.query('UPDATE usuario SET nombre = $1, apellidos = $2, correo = $3, pasword = $4 WHERE id_usuario = $5', [
            nombre, apellidos, correo, pasword, id
        ]);

        res.status(200).json({
            message: 'User Updated Successfully',
            data: response.oid
        });
    } catch (error) {
        res.status(500).json({
            message: 'User NOT Updated Successfully',
        });
    }

};
export const deleteUser = async (req: Request, res: Response) => {
    const id = req.params.id;
    console.log(id);
    try {
        await pool.query('DELETE FROM usuario where id_usuario = $1', [
            id
        ]).then(
            (data)=>{
                res.status(200).json(`User ${id} deleted Successfully`);
            }
        ).catch(
            (err)=>{
                res.status(400).json({ message: "Internal Server Error", err: err });
            }
        );

    } catch (error) {
        res.status(400).json({ message: "Internal Server Error", err: error });
    }
};

