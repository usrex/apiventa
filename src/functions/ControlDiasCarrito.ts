export function sumarDias(fecha:Date, dias:number){
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }