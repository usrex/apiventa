import express from 'express';
import morgan from 'morgan';
import router from './routes/index';
const app = express();
const port = process.env.PORT|| 3000;

//midleware
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(morgan('dev'));
// routes
app.use(router);
//interface
app.set('port', port);


export default app;
