import { Router } from "express";
import { TokenValidator } from "../libs/veryfiToken";
import { getproductcategory, getProduct, addProduct, addTalla, addPrecioColor, EditTPrecio, editProducto, deleteProducto} from "../controller/controller.producto";

const router = Router();

router.get('/producto',TokenValidator, getProduct);
router.get('/producto/categoria',TokenValidator, getproductcategory);

router.post('/producto/add',TokenValidator, addProduct);
router.post('/producto/add/talla/color',TokenValidator, addPrecioColor);
router.post('/producto/add/talla/:id',TokenValidator,addTalla);

router.put('/producto/edit/precio/:id_producto',TokenValidator, EditTPrecio);
router.put('/producto/edit/producto/:id_producto',TokenValidator, editProducto);

router.delete('/producto/delete/:id_producto',TokenValidator, deleteProducto);

/*
router.get('/producto/:id', getUserById);
router.post('/producto', createUser);
router.put('/producto/:id', updateUser);
//'/usuarios/:id?/:id_2' mas parametros 
router.delete('/producto/:id', deleteUser);
*/

export default router;