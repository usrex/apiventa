import { Router } from "express";
import { profile, signin, signup,editProfile,deleteUser } from "../controller/controller.auth";
import { TokenValidator } from "../libs/veryfiToken";
const router: Router = Router();

router.post('/signup',signup);
router.post('/signin',signin);
router.get('/profile',TokenValidator,profile);
router.post('/profile',TokenValidator,editProfile);
router.delete('/profile/delete/:id',TokenValidator,deleteUser);


export default router;