import { Router } from "express";
import { TokenValidator } from "../libs/veryfiToken";
import { getCarritoUserNotValidate, getCarritoUserValidate, addCarrito, addProductCarrito} from "../controller/controller.carrito";

const router = Router();

router.get('/carrito/validate/:id',TokenValidator, getCarritoUserValidate);
router.get('/carrito/notValidate/:id',TokenValidator, getCarritoUserNotValidate);
router.post('/carrito/createCarrito/:id',TokenValidator,addCarrito );
router.post('/carrito/addproduct',TokenValidator,addProductCarrito );


export default router;