import { Router } from "express";
import { TokenValidator } from "../libs/veryfiToken";
import { getUsers } from "../controller/controller.usuario";

const router = Router();

router.get('/usuarios',TokenValidator ,getUsers);
/*
router.get('/usuarios/:id', getUserById);
router.post('/usuarios', createUser);
router.put('/usuarios/:id', updateUser);
//'/usuarios/:id?/:id_2' mas parametros 
router.delete('/usuarios/:id', deleteUser);
*/



export default router;