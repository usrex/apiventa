import { Router,Response } from "express";

import routerUser from "../routes/routes.usuario";
import routerProduct from "../routes/routes.producto";
import auth from "../routes/auth";
import routeCarrito from "../routes/route.carrito";
const router: Router = Router();


//router.get('/',(res:Response)=>{return res.send("WelcomeToApiVentasLeyvaFashion")});
router.use(auth);
router.use(routerUser);
router.use(routerProduct);
router.use(routeCarrito);

export default router;