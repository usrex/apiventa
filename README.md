# iniciar projecto con node y typescript
-- comandos
> npm init -y
> npm i typescript -D

-- integrar typescript al projecto
 > npx tsc --init

 -- modulos a instalar
 > express  pg nodemon concurrently



### levantar el servicio 
# paso 1
> npm i // permite descargar los paquetes
# paso 2 levantamos el servidor una ves corriento [Tener en cuenta tener la base de datos con el nombre "Dev_ventas" y las tablas de la carpeta database ]
> npm run dev // levanta el servidor en direccion localhost:3000



## Rutas

- get
> http://localhost:3000/usuarios  // devuele usuarios

    {
        "id_usuario": 5,
        "nombre": "prueba1",
        "apellidos": "ap",
        "correo": "prueba@prueba.com",
        "pasword": "123456798",
        "rol_usuario": "Usuario",
        "url_image": "www.imagen.jpg"
    },

> http://localhost:3000/usuarios/#id_usuario sacar usuario por id

     ej. http://localhost:3000/usuarios/1

    {
        "id_usuario": 5,
        "nombre": "prueba1",
        "apellidos": "ap",
        "correo": "prueba@prueba.com",
        "pasword": "123456798",
        "rol_usuario": "Usuario",
        "url_image": "www.imagen.jpg"
    },

- post
>http://localhost:3000/usuarios // insertar
headers
    {Content-Type:application/json} // requerido

    formato de envio contenido
    {
        "nombre":"prueba33",
        "apellidos":"ap",
        "correo":"prueba33@prueba.com",
        "pasword":"123456798",
        "rol_usuario":"Usuario",
        "url_image":"www.imagen.jpg"
    }