"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const veryfiToken_1 = require("../libs/veryfiToken");
const controller_producto_1 = require("../controller/controller.producto");
const router = express_1.Router();
router.get('/producto', veryfiToken_1.TokenValidator, controller_producto_1.getProduct);
router.get('/producto/categoria', veryfiToken_1.TokenValidator, controller_producto_1.getproductcategory);
router.post('/producto/add', veryfiToken_1.TokenValidator, controller_producto_1.addProduct);
router.post('/producto/add/talla/color', veryfiToken_1.TokenValidator, controller_producto_1.addPrecioColor);
router.post('/producto/add/talla/:id', veryfiToken_1.TokenValidator, controller_producto_1.addTalla);
router.put('/producto/edit/precio/:id_producto', veryfiToken_1.TokenValidator, controller_producto_1.EditTPrecio);
router.put('/producto/edit/producto/:id_producto', veryfiToken_1.TokenValidator, controller_producto_1.editProducto);
router.delete('/producto/delete/:id_producto', veryfiToken_1.TokenValidator, controller_producto_1.deleteProducto);
/*
router.get('/producto/:id', getUserById);
router.post('/producto', createUser);
router.put('/producto/:id', updateUser);
//'/usuarios/:id?/:id_2' mas parametros
router.delete('/producto/:id', deleteUser);
*/
exports.default = router;
//# sourceMappingURL=routes.producto.js.map