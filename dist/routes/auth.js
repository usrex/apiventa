"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const controller_auth_1 = require("../controller/controller.auth");
const veryfiToken_1 = require("../libs/veryfiToken");
const router = express_1.Router();
router.post('/signup', controller_auth_1.signup);
router.post('/signin', controller_auth_1.signin);
router.get('/profile', veryfiToken_1.TokenValidator, controller_auth_1.profile);
router.post('/profile', veryfiToken_1.TokenValidator, controller_auth_1.editProfile);
router.delete('/profile/delete/:id', veryfiToken_1.TokenValidator, controller_auth_1.deleteUser);
exports.default = router;
//# sourceMappingURL=auth.js.map