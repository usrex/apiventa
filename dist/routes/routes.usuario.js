"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const veryfiToken_1 = require("../libs/veryfiToken");
const controller_usuario_1 = require("../controller/controller.usuario");
const router = express_1.Router();
router.get('/usuarios', veryfiToken_1.TokenValidator, controller_usuario_1.getUsers);
/*
router.get('/usuarios/:id', getUserById);
router.post('/usuarios', createUser);
router.put('/usuarios/:id', updateUser);
//'/usuarios/:id?/:id_2' mas parametros
router.delete('/usuarios/:id', deleteUser);
*/
exports.default = router;
//# sourceMappingURL=routes.usuario.js.map