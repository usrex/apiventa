"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const veryfiToken_1 = require("../libs/veryfiToken");
const controller_carrito_1 = require("../controller/controller.carrito");
const router = express_1.Router();
router.get('/carrito/validate/:id', veryfiToken_1.TokenValidator, controller_carrito_1.getCarritoUserValidate);
router.get('/carrito/notValidate/:id', veryfiToken_1.TokenValidator, controller_carrito_1.getCarritoUserNotValidate);
router.post('/carrito/createCarrito/:id', veryfiToken_1.TokenValidator, controller_carrito_1.addCarrito);
router.post('/carrito/addproduct', veryfiToken_1.TokenValidator, controller_carrito_1.addProductCarrito);
exports.default = router;
//# sourceMappingURL=route.carrito.js.map