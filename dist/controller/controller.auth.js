"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.editProfile = exports.profile = exports.signin = exports.signup = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const database_1 = require("../database");
const encryptPassword_1 = require("../functions/encryptPassword");
exports.signup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { nombre, apellidos, correo, rol_usuario, url_image } = req.body;
    const pasword = yield encryptPassword_1.encrypt(req.body.pasword);
    try {
        const response = yield database_1.pool.query('insert into Usuario(nombre, apellidos, correo, pasword, rol_usuario, url_image) VALUES ($1, $2,$3,$4,$5,$6)', [nombre, apellidos, correo, pasword, rol_usuario, url_image]);
        //console.log(response);
        try {
            const querysearchid = yield database_1.pool.query('SELECT * FROM Usuario WHERE nombre = $1 and correo = $2 and pasword = $3 and rol_usuario = $4', [nombre, correo, pasword, rol_usuario]);
            const id = querysearchid.rows.map((data) => {
                return data.id_usuario;
            });
            //console.log(id[0]);
            const token = jsonwebtoken_1.default.sign({ id: id[0] }, "TOkEN_TESTING");
            return res.header("token-auth", token).status(200).json({
                message: "usuario agregado con exito",
                body: { nombre, apellidos, correo, rol_usuario, url_image, pasword },
                numeroInserts: response.rows
            });
        }
        catch (error) {
            console.log("data no encontrada");
            return res.status(500).json("Internal Server Error");
        }
    }
    catch (error) {
        console.log("no insert");
        return res.status(500).json("internal server error");
    }
});
exports.signin = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { correo, pasword } = req.body;
    console.log(correo);
    console.log(pasword);
    try {
        const response = yield database_1.pool.query('SELECT * FROM Usuario WHERE correo = $1', [correo]);
        const passwordCrypt = response.rows.map((data) => { return { pass: data.pasword, id: data.id_usuario }; });
        //console.log(passwordCrypt);
        const compar = yield encryptPassword_1.comparePassword(pasword, passwordCrypt[0].pass); // ? errore compare 
        if (compar) {
            const token = jsonwebtoken_1.default.sign({ id: passwordCrypt[0].id }, "TOkEN_TESTING");
            return res.header('auth-token', token)
                .status(200)
                .json({
                message: "inicio con exito",
                data: response.rows
            });
        }
        else
            return res.status(400).json("contraseño o email incorrectos");
    }
    catch (error) {
        return res.send({ message: 'internal server error', error: error });
    }
});
exports.profile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //console.log(req.headers['auth-token']);
    //console.log(req.userId);
    try {
        const response = yield database_1.pool.query('SELECT * FROM Usuario WHERE id_usuario = $1', [req.userId]);
        if (response.rowCount != 0) {
            return res.status(200).json(response.rows);
        }
        else
            return res.status(404).json('Data not found');
    }
    catch (error) {
        return res.status(400).json('internal server error');
    }
});
exports.editProfile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("put user");
    // actualizar
    // nombre,apellidos,correo, pasword
    const id = parseInt(req.userId);
    const { nombre, apellidos, correo, pasword } = req.body;
    //console.log(id);
    //console.log(nombre,apellidos,correo, pasword);
    try {
        const response = yield database_1.pool.query('UPDATE usuario SET nombre = $1, apellidos = $2, correo = $3, pasword = $4 WHERE id_usuario = $5', [
            nombre, apellidos, correo, pasword, id
        ]);
        res.status(200).json({
            message: 'User Updated Successfully',
            data: response.oid
        });
    }
    catch (error) {
        res.status(500).json({
            message: 'User NOT Updated Successfully',
        });
    }
});
exports.deleteUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = req.params.id;
    console.log(id);
    try {
        yield database_1.pool.query('DELETE FROM usuario where id_usuario = $1', [
            id
        ]).then((data) => {
            res.status(200).json(`User ${id} deleted Successfully`);
        }).catch((err) => {
            res.status(400).json({ message: "Internal Server Error", err: err });
        });
    }
    catch (error) {
        res.status(400).json({ message: "Internal Server Error", err: error });
    }
});
//# sourceMappingURL=controller.auth.js.map